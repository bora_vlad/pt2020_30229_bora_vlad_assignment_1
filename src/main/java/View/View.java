package View;


import Model.*;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;

public class View  extends JFrame {

    private JTextField textFieldFirstPoly;
    private JTextField textFieldSecondPoly;
    private JTextField textFieldRezFirst;
    private JTextField textFieldRezSecond;

    private JButton btnAdunare;
    private JButton btnScadere;
    private JButton btnInmultire;
    private JButton btnImpartire;
    private JButton btnDerivare;
    private JButton btnIntegrare;

    private JLabel lblError;

    public View(Model model){

        JFrame frame;

        JLabel lblNewLabel;
        JLabel lblRezultat;
        JLabel lblCat;

        frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(100, 100, 650, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        lblNewLabel = new JLabel("Polynomials:");
        lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        lblNewLabel.setBounds(30, 30, 150, 25);
        frame.getContentPane().add(lblNewLabel);

        lblError = new JLabel("");
        lblError.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        lblError.setBounds(250, 30, 250, 25);
        lblError.setForeground(Color.red);
        frame.getContentPane().add(lblError);

        textFieldFirstPoly = new JTextField();
        textFieldFirstPoly.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        textFieldFirstPoly.setBounds(30, 75, 250, 30);
        frame.getContentPane().add(textFieldFirstPoly);
        textFieldFirstPoly.setColumns(10);

        btnAdunare = new JButton("Adunare");
        btnAdunare.setBackground(Color.WHITE);
        btnAdunare.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnAdunare.setBounds(30, 150, 130, 40);
        frame.getContentPane().add(btnAdunare);

        btnScadere = new JButton("Scadere");
        btnScadere.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnScadere.setBackground(Color.WHITE);
        btnScadere.setBounds(250, 150, 130, 40);
        frame.getContentPane().add(btnScadere);

        btnInmultire = new JButton("Inmultire");
        btnInmultire.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnInmultire.setBackground(Color.WHITE);
        btnInmultire.setBounds(30, 225, 130, 40);
        frame.getContentPane().add(btnInmultire);

        btnImpartire = new JButton("Impartire");
        btnImpartire.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnImpartire.setBackground(Color.WHITE);
        btnImpartire.setBounds(250, 225, 130, 40);
        frame.getContentPane().add(btnImpartire);

        btnDerivare = new JButton("Derivare");
        btnDerivare.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnDerivare.setBackground(Color.WHITE);
        btnDerivare.setBounds(475, 150, 130, 40);
        frame.getContentPane().add(btnDerivare);

        btnIntegrare = new JButton("Integrare");
        btnIntegrare.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnIntegrare.setBackground(Color.WHITE);
        btnIntegrare.setBounds(475, 225, 130, 40);
        frame.getContentPane().add(btnIntegrare);

        textFieldSecondPoly = new JTextField();
        textFieldSecondPoly.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        textFieldSecondPoly.setColumns(10);
        textFieldSecondPoly.setBounds(350, 75, 250, 30);
        frame.getContentPane().add(textFieldSecondPoly);

        textFieldRezFirst = new JTextField();
        textFieldRezFirst.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        textFieldRezFirst.setColumns(10);
        textFieldRezFirst.setBounds(30, 315, 250, 30);
        frame.getContentPane().add(textFieldRezFirst);

        textFieldRezSecond = new JTextField();
        textFieldRezSecond.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        textFieldRezSecond.setColumns(10);
        textFieldRezSecond.setBounds(350, 315, 250, 30);
        frame.getContentPane().add(textFieldRezSecond);

        lblRezultat = new JLabel("Rezultat:");
        lblRezultat.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        lblRezultat.setBounds(30, 275, 150, 30);
        frame.getContentPane().add(lblRezultat);

        lblCat = new JLabel("Cat:");
        lblCat.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        lblCat.setBounds(350, 275, 150, 30);

        frame.getContentPane().add(lblCat);
        frame.setTitle("CevaTitlu");
        frame.setVisible(true);
    }

    public void addBtnIntegrareListener(ActionListener action) {
        btnIntegrare.addActionListener(action);
    }

    public void addBtnDerivateListener(ActionListener action) {
        btnDerivare.addActionListener(action);
    }

    public void addBtnAdunareListener(ActionListener action) {
        btnAdunare.addActionListener(action);
    }

    public void addBtnScadereListener(ActionListener action) {
        btnScadere.addActionListener(action);
    }

    public void addBtnImpartireListener(ActionListener action) {
        btnImpartire.addActionListener(action);
    }

    public void addBtnInmultireListener(ActionListener action) {
        btnInmultire.addActionListener(action);
    }

    public String getFirstPoly() {
        return textFieldFirstPoly.getText();
    }

    public void setLblError(String text) {
        this.lblError.setText(text);
    }

    public String getSecondPoly() {
        return textFieldSecondPoly.getText();
    }

    public void setTextFieldRezFirst(String text) {
        this.textFieldRezFirst.setText(text);
    }

    public void setTextFieldRezSecond(String text) {
        this.textFieldRezSecond.setText(text);
    }


}
