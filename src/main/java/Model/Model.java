package Model;

import java.util.TreeSet;

public class Model
{
    public Polynomial addPolynom(Polynomial first, Polynomial second) {
        Polynomial myPolynomial = new Polynomial();
        for (Monomial itFirst : first.getMonoame())
        {
            boolean ok  = true;
            for (Monomial itSecond : second.getMonoame())
            {
                if(itSecond.getPower() == itFirst.getPower())
                {
                    ok = false;
                    myPolynomial.addMonom(itFirst.addMonomial(itSecond));
                }
            }
            if (ok)
            {
                myPolynomial.addMonom(new Monomial(itFirst.getCoef(), itFirst.getPower()));
            }
        }

        for (Monomial itSecond :  second.getMonoame())
        {
            boolean ok = true;
            for (Monomial itFirst : first.getMonoame())
            {
                if(itSecond.getPower() == itFirst.getPower())
                {
                    ok = false;
                }
            }
            if (ok)
            {
                myPolynomial.addMonom(new Monomial(itSecond.getCoef(), itSecond.getPower()));
            }
        }
        return myPolynomial;
    }

    public Polynomial subPolynom(Polynomial first, Polynomial second) {
        Polynomial myPolynomial = new Polynomial();
        for (Monomial itFirst : first.getMonoame()) {
            boolean ok  = true;
            for (Monomial itSecond : second.getMonoame()) {
                if(itSecond.getPower() == itFirst.getPower()) {
                    ok = false;
                    myPolynomial.addMonom(itFirst.subMonomial(itSecond));
                }
            }
            if (ok) {
                myPolynomial.addMonom(new Monomial( itFirst.getCoef(), itFirst.getPower()));
            }
        }

        for (Monomial itSecond : second.getMonoame()) {
            boolean ok = true;
            for (Monomial itFirst : first.getMonoame()) {
                if(itSecond.getPower() == itFirst.getPower()) {
                    ok = false;
                }
            }
            if (ok) {
                myPolynomial.addMonom(new Monomial(-1 * itSecond.getCoef(), itSecond.getPower()));
            }
        }

        Polynomial newPoly = new Polynomial();
        for (Monomial temp : myPolynomial.getMonoame()) {
            if(temp.getCoef() != 0) {
                newPoly.addMonom(temp);
            }
        }
        return newPoly;
    }

    public Polynomial multiplyPolynom(Polynomial first, Polynomial second){
        Polynomial myPolynomial = new Polynomial();
        for (Monomial itFirst : first.getMonoame()) {
            for (Monomial itSecond :second.getMonoame()) {
                Monomial interm = itFirst.multMonomial(itSecond);
                boolean ok = true;
                for (Monomial itThird : myPolynomial.getMonoame()){
                    if (itThird.getPower() == interm.getPower())
                    {
                        itThird.increaseMonomial(interm);
                        ok = false;
                    }
                }
                if (ok)
                {
                    myPolynomial.addMonom(interm);
                }
            }
        }
        return myPolynomial;
    }

    public Polynomial derivatePolynom(Polynomial first) {
        Polynomial myPolynom = new Polynomial();
        for(Monomial itFirst : first.getMonoame()) {
            Monomial mon = itFirst.derivateMonomial();
            myPolynom.addMonom(mon);
        }
        return myPolynom;
    }

    public Polynomial integratePolynom(Polynomial first) {
        Polynomial myPolynom = new Polynomial();
        for(Monomial itFirst : first.getMonoame()) {
            Monomial mon = itFirst.integrateeMonomial();
            myPolynom.addMonom(mon);
        }
        return myPolynom;
    }

    public void impartirePolynom(Polynomial first, Polynomial second, Polynomial cat, Polynomial rest) {
        Polynomial temp = new Polynomial();
        for (Monomial myMon : first.getMonoame()) {
            temp.addMonom(myMon);
        }
        Polynomial myPoly = new Polynomial();

        try {
            while(temp.getGradMaxim() >= second.getGradMaxim()) {
                myPoly.addMonom(((Monomial) temp.getMonoame().first()).divideMonomia((Monomial) second.getMonoame().first()));
                cat.addMonom((Monomial) myPoly.getMonoame().first());
                temp = subPolynom(temp, multiplyPolynom(myPoly, second));
                myPoly.getMonoame().remove(myPoly.getMonoame().first());
            }
        }
        catch (Exception e) {

        }
        for (Monomial mon : temp.getMonoame()) {
            rest.addMonom(mon);
        }
    }
}
