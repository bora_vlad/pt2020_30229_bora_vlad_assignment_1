package Model;

public class Monomial implements Comparable
{
    private double Coef;
    private int Power;

    public Monomial() {

    }

    public Monomial(double Coef, int Power) {
        this.Coef = Coef;
        this.Power = Power;
    }

    public double getCoef() {
        return Coef;
    }

    public int getPower() {
        return Power;
    }

    public void setCoef(double coef) {
        Coef = coef;
    }

    public void setPower(int power) {
        Power = power;
    }

    public Monomial addMonomial(Monomial other) {
        Monomial newMonomial = new Monomial();
        newMonomial.Coef = other.Coef + this.Coef;
        newMonomial.Power = other.Power;
        return newMonomial;
    }

    public Monomial subMonomial(Monomial other) {
        Monomial newMonomial = new Monomial();
        newMonomial.Coef = this.Coef - other.Coef;
        newMonomial.Power = other.Power;
        return newMonomial;
    }

    public Monomial multMonomial(Monomial other) {
        Monomial newMonomial =new Monomial();
        newMonomial.Coef = this.Coef * other.Coef;
        newMonomial.Power = this.Power + other.Power;
        return newMonomial;
    }

    public Monomial derivateMonomial() {
        Monomial newMonomial = new Monomial();
        newMonomial.Coef = this.Coef * this.Power;
        if (this.Power > 0)
        {
            newMonomial.Power = this.Power - 1;
        }
        return newMonomial;
    }

    public Monomial integrateeMonomial() {
        Monomial newMonomial = new Monomial();
        newMonomial.Power = this.Power + 1;
        newMonomial.Coef = this.Coef / newMonomial.Power;
        return newMonomial;
    }

    public void increaseMonomial(Monomial other) {
        if (this.Power == other.Power) {
            this.Coef += other.Coef;
        }
    }

    public int compareTo(Object obj) {
        return -1 * Integer.valueOf(this.Power).compareTo(Integer.valueOf(((Monomial)obj).Power));
    }

    public String toString() {
        String s = "";
        if (this.Coef == 0)
        {
            return "0";
        }
        if(this.Coef > 0)
            s = "+";
        s += this.Coef;
        if (this.Power == 0)
        {
            return s;
        }
            s += "x";
        if (this.Power == 1)
            return s;
        s += "^" + this.Power;
        return s;
    }

    public Monomial divideMonomia(Monomial first) {
        Monomial rezultat = new Monomial();
        rezultat.Coef = this.Coef / first.Coef;
        rezultat.Power = this.Power - first.Power;
        return rezultat;
    }

}
