package Controller;

import Model.*;
import View.View;

import java.awt.event.*;

public class Controller {

    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        view.addBtnAdunareListener(new AdunareListener());
        view.addBtnScadereListener(new ScadereListener());
        view.addBtnDerivateListener(new DerivareListener());
        view.addBtnIntegrareListener(new IntegrareListener());
        view.addBtnImpartireListener(new ImpartireListener());
        view.addBtnInmultireListener(new InmultireListener());
    }

    class AdunareListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (view.getFirstPoly().isEmpty() || view.getSecondPoly().isEmpty())
            {
                view.setLblError("Ambele campuri trebuie completate");
                return;
            }
            Polynomial firstPoly = new Polynomial(view.getFirstPoly());
            Polynomial secondPoly = new Polynomial(view.getSecondPoly());

            view.setTextFieldRezFirst(model.addPolynom(firstPoly, secondPoly).toString());
            view.setLblError("");
        }
    }


    class ScadereListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (view.getFirstPoly().isEmpty() || view.getSecondPoly().isEmpty())
            {
                view.setLblError("Ambele campuri trebuie completate");
                return;
            }
            Polynomial firstPoly = new Polynomial(view.getFirstPoly());
            Polynomial secondPoly = new Polynomial(view.getSecondPoly());

            view.setTextFieldRezFirst(model.subPolynom(firstPoly, secondPoly).toString());
            view.setLblError("");
        }
    }


    class DerivareListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (view.getFirstPoly().isEmpty())
            {
                view.setLblError("Primul camp trebuie completat");
                return;
            }
            Polynomial firstPoly = new Polynomial(view.getFirstPoly());
            view.setTextFieldRezFirst(model.derivatePolynom(firstPoly).toString());
            view.setLblError("");

        }
    }


    class IntegrareListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (view.getFirstPoly().isEmpty())
            {
                view.setLblError("Primul camp trebuie completat");
                return;
            }
            Polynomial firstPoly = new Polynomial(view.getFirstPoly());

            view.setTextFieldRezFirst(model.integratePolynom(firstPoly).toString());
            view.setLblError("");
        }
    }


    class ImpartireListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (view.getFirstPoly().isEmpty() || view.getSecondPoly().isEmpty())
            {
                view.setLblError("Ambele campuri trebuie completate");
                return;
            }
            Polynomial firstPoly = new Polynomial(view.getFirstPoly());
            Polynomial secondPoly = new Polynomial(view.getSecondPoly());
            Polynomial cat = new Polynomial();
            Polynomial rest = new Polynomial();
            model.impartirePolynom(firstPoly, secondPoly, cat, rest);
            view.setTextFieldRezFirst(cat.toString());
            view.setTextFieldRezSecond(rest.toString());
            view.setLblError("");
        }
    }


    class InmultireListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (view.getFirstPoly().isEmpty() || view.getSecondPoly().isEmpty())
            {
                view.setLblError("Ambele campuri trebuie completate");
                return;
            }
            Polynomial firstPoly = new Polynomial(view.getFirstPoly());
            Polynomial secondPoly = new Polynomial(view.getSecondPoly());

            view.setTextFieldRezFirst(model.multiplyPolynom(firstPoly, secondPoly).toString());
            view.setLblError("");

        }
    }

}
