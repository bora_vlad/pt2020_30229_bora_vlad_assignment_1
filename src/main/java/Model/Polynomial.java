package Model;

import Model.Monomial;

import java.util.TreeSet;
import java.util.regex.*;

public class Polynomial  {
    private TreeSet<Monomial> monoame ;

    public TreeSet<Monomial> getMonoame() {
        return monoame;
    }

    public Polynomial() {
        monoame = new TreeSet<>();
    }

    public void addMonom(Monomial other) {
        monoame.add(other);
    }

    public Polynomial(String polinom) {
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)" );
        Matcher matcher = pattern.matcher(polinom);
        this.monoame = new TreeSet<Monomial>();
        while(matcher.find()) {
            Pattern monomPattern = Pattern.compile("([+-]?\\d*[.]?\\d*)[xX]?\\^?(\\d*)");
            Matcher monomMatcher = monomPattern.matcher(matcher.group(1).toString());
            monomMatcher.find();
            int putereMonom = 0;
            double coefMonom = 0;
            if (monomMatcher.group(2).isEmpty()) {
                putereMonom = 0;
            }
            else {
                putereMonom = Integer.parseInt(monomMatcher.group(2));
            }
            if (monomMatcher.group(1).equals("+") || monomMatcher.group(1).isEmpty()) {
                coefMonom = 1;
            }
            else
                if(monomMatcher.group(1).equals("-")) {
                    coefMonom = -1;
                }
                else {
                    coefMonom = Double.parseDouble(monomMatcher.group(1));
                }
            Monomial monom = new Monomial(coefMonom, putereMonom);
            monoame.add(monom);
        }
    }

    public String toString() {
        String s = "";
        for (Monomial itFirst : this.monoame) {
            s += itFirst.toString();
        }
        return s;
    }

    public int getGradMaxim() {
        return monoame.first().getPower();
    }


}
