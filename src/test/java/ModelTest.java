import Model.Model;
import org.junit.jupiter.api.Test;
import Model.*;

import static org.junit.jupiter.api.Assertions.*;

class ModelTest {

    Model model = new Model();

    @Test
    void addPolynom() {
        Polynomial first = new Polynomial("4x^2+3x^1+1");
        Polynomial second = new Polynomial("7x^3-40");
        Polynomial myPoly = model.addPolynom(first, second);
        assertEquals("+7.0x^3+4.0x^2+3.0x-39.0", myPoly.toString());
    }

    @Test
    void subPolynom() {
        Polynomial first = new Polynomial("4x^2+3x^1+1");
        Polynomial second = new Polynomial("7x^3-40");
        Polynomial myPoly = model.subPolynom(first, second);
        assertEquals("-7.0x^3+4.0x^2+3.0x+41.0", myPoly.toString());
    }

    @Test
    void multiplyPolynom() {
        Polynomial first = new Polynomial("4x^2+3x^1+1");
        Polynomial second = new Polynomial("7x^3-40");
        Polynomial myPoly = model.multiplyPolynom(first, second);
        assertEquals("+28.0x^5+21.0x^4+7.0x^3-160.0x^2-120.0x-40.0", myPoly.toString());
    }

    @Test
    void derivatePolynom() {
        Polynomial first = new Polynomial("5x^3-4x^2-3x^1+10");
        Polynomial myPoly = model.derivatePolynom(first);
        assertEquals("+15.0x^2-8.0x-3.0", myPoly.toString());
    }

    @Test
    void integratePolynom() {
        Polynomial first = new Polynomial("3x^3-6x^2-4x^1+10");
        Polynomial myPoly = model.integratePolynom(first);
        assertEquals("+0.75x^4-2.0x^3-2.0x^2+10.0x", myPoly.toString());
    }

    @Test
    void impartirePolynom() {
        Polynomial first = new Polynomial("+3x^2+x^1+1");
        Polynomial second = new Polynomial("+x^1+1");
        Polynomial rez = new Polynomial();
        Polynomial cat = new Polynomial();
        model.impartirePolynom(first, second, rez, cat);
        assertEquals("+3.0x-2.0", rez.toString());
        assertEquals("+3.0", cat.toString());
    }
}